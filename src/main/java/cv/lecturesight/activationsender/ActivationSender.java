package cv.lecturesight.activationsender;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import joptsimple.OptionParser;
import joptsimple.OptionSet;

/**
 *
 * @author wulff
 */
public class ActivationSender {

    private final static String MESSAGE_PREFIX = "::TRACK:";

    // parameters for operation 
    private InetAddress targetIP = null;    // host to send the messages to
    private String clientID = "default";    // client ID used in massages
    private int interval = 2000;            // interval between messages in milliseconds"-h", "-c", "-i"
    private int count = -1;

    private DatagramSocket socket;

    void parseArguments(String[] args) {
        OptionParser parser = new OptionParser("h:c:i:n:");
        OptionSet options = parser.parse(args);

        // target IP address
        try {
            if (options.has("h")) {
                if (options.hasArgument("h")) {
                    targetIP = InetAddress.getByName((String) options.valueOf("h"));
                } else {
                    throw new RuntimeException("Option -h requires hostname or IP address as argument");
                }
            } else {
                targetIP = InetAddress.getByName("localhost");
            }
        } catch (UnknownHostException e) {
            throw new RuntimeException("Unable to resolve hostname", e);
        }

        // client ID
        if (options.has("c")) {
            if (options.hasArgument("c")) {
                clientID = (String) options.valueOf("c");
            } else {
                throw new RuntimeException("Option -c requires client ID as argument");
            }
        }

        // interval
        if (options.has("i")) {
            if (options.hasArgument("i")) {
                try {
                    interval = Integer.parseInt((String) options.valueOf("i"));
                } catch (NumberFormatException e) {
                    throw new RuntimeException("Unable to parse interval", e);
                }
            } else {
                throw new RuntimeException("Option -i requires interval between messages in milliseconds");
            }
        }

        // count
        if (options.has("n")) {
            if (options.hasArgument("n")) {
                try {
                    count = Integer.parseInt((String) options.valueOf("n"));
                } catch (NumberFormatException e) {
                    throw new RuntimeException("Unable to parse count", e);
                }
            } else {
                throw new RuntimeException("Option -n requires number of times the message should be send");
            }
        }
    }
    

    public void main(String[] argv) {
        parseArguments(argv);

        String message = MESSAGE_PREFIX + clientID;
        byte[] data = message.getBytes();
        
        try {
            socket = new DatagramSocket();
        } catch (SocketException e) {
            throw new RuntimeException("Unable to initialize datagram socket.", e);
        }

        System.out.println("Sending message: " + message);
        System.out.println("to: " + targetIP.toString());
        System.out.println("interval:" + interval + "ms");
        
        if (count != -1) {
            System.out.println(count + " times");
        }

        do {
            DatagramPacket packet;
            packet = new DatagramPacket(data, data.length, targetIP, 2501);
            try {
                socket.send(packet);
                System.out.print(".");
            } catch (IOException e) {
                throw new RuntimeException("Unable to send UDP packet.", e);
            }
            count--;
        } while (count != 0);
    }

}
